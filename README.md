# Opensearch Dashboard Insecure

This repo creates an OpenSearch Dashboard container image where
security is turned of. It uses plain HTTP and do not have
authentication turned on.

It is intended to ease usage for developers.
**DO NOT USE IT IN PRODUCTION**

## Run

If you do not have an OpenSearch server running, start one with:

```shell
podman run -p 9200:9200 -p 9600:9600 -e "discovery.type=single-node" -e "plugins.security.disabled=true" opensearchproject/opensearch:1.3.1
```

Then start the dashboard:

```shell
podman run -p 5601:5601 -e "OPENSEARCH_HOSTS=http://192.168.0.100:9200" docker-images.jobtechdev.se/opensearch-dashboard-insecure/opensearch-dashboard-insecure:1.3.0
```

Replace the IP-number with the IP of your OpenSearch server. On mac and
windows use the hostname `host.docker.internal`.

If you are using Docker, replace `podman` with `docker`.

Once both containers has started go to `http://localhost:5601` to get the dashboard UI and
`http://localhost:9200` to access OpenSearch database.

## Slow OpenSearch

If your OpenSearch is slow, try to increase the memory.

```shell
podman run -p 9200:9200 -p 9600:9600 -e "OPENSEARCH_JAVA_OPTS=-Xms2048m -Xmx2048m" -e "discovery.type=single-node" -e "plugins.security.disabled=true" opensearchproject/opensearch:1.3.1
```

If you run Docker on Windows or Mac, verify that your virtual machine running the Docker containers
has more memory assigned than your containers requires.

## On Mac with Docker "OpenSearch exited with code 137"

There are not enough memory mapping area in the Docker VM.

Do:
```shell
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
exit
``` 


