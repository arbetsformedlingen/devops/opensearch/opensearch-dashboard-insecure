FROM docker.io/opensearchproject/opensearch-dashboards:1.3.12

COPY opensearch_dashboards.yml /usr/share/opensearch-dashboards/config/

RUN bin/opensearch-dashboards-plugin remove securityDashboards
